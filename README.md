# ンソールオープンソース

## 描写

オープンソースのコンソールを設計するために、私はこのリポジトリを作成しました。私の好きなPCBはこれです

* Raspberry PI zero/3/4
* Arduino UNO,NANO,METRO


## RPI

コンピューターみたいだから、複雑なプロジェクトを作成するためにRPIは完璧です。さらにGPIにコンポーネントを接続することができます。

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages.techhive.com%2Fimages%2Farticle%2F2015%2F02%2Fraspberry-pi-2-sd-card-100569129-orig.png&f=1&nofb=1)


ビジュアルノベルゲームを書くために[RENPY](https://www.renpy.org/)はいいです。RPIはLINUXをありますだからそのマイクロコントローラーを使うことができます

![](https://gitlab.com/terceranexus6/opensourceconsole/-/raw/master/images/sket1.jpg)
![](https://gitlab.com/terceranexus6/opensourceconsole/-/raw/master/images/sket_2.jpg)
